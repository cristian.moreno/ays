
import React from 'react';
import './style.css';

function Currency(input) {
  const { text, code, handlerChange, is_calculated, formula, form, display } = { ...input };
  const value = formula && formula(form);
  return (
    <React.Fragment>
      {
        display(form) === 'show'
          ? (
            <div className="form-group row">
              <label className="col-sm-4 col-form-labe">
                {text}
              </label>
              <div className="col-sm-8">
                {
                  is_calculated
                  ? (
                    <input
                      type="text"
                      className="form-control"
                      value={isNaN(value) ? 0 : value}
                      name={code}
                      disabled={is_calculated}
                      placeholder="Enter here"
                    />
                  )
                  : (
                    <input
                      type="number"
                      className="form-control"
                      name={code}
                      onChange={handlerChange}
                      placeholder="Enter here"
                    />
                  )
                }
              </div>
            </div>
          )
          : null
      }
    </React.Fragment>
  );
};

export default Currency;
