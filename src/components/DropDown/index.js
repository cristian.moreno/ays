
import React from 'react';

function DropDown(input) {
  const { text, handlerChange, values = {}, code } = { ...input };
  const data = Object.keys(values).map((key) => ({ text: key, value: values[key] }));

  return (
    <div className="form-group row">
      <label className="col-sm-4 col-form-labe">
        {text}
      </label>
      <div className="col-sm-8">
        <select
          name={code}
          onChange={handlerChange}
          className="form-control"
        >
          {
            data.map((item, index) => (
              <option
                key={index}
                value={item.value}>
                {item.text}
              </option>
            ))
          }
        </select>
      </div>
    </div>
  );
};

export default React.memo(DropDown);
