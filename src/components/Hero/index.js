import React from 'react';
import {
  BannerPart,
  BannerText,
  BannerTextInner,
  Hey,
  Title,
  Description,
  Button,
} from './style';

function Hero({ handler }) {
  return (
    <BannerPart>
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-6">
            <BannerText>
              <BannerTextInner>
                <Hey>Hey</Hey>
                <Title>Welcome</Title>
                <Description>
                  Lorem ipsum dolor sit amet consectetur adipiscing elitsed do eiusmod tempor incididunt ut
                  labore et dolore.
                </Description>
                <div className="banner_btn">
                  <Button onClick={handler}>
                    Let's Go <span role="img" aria-label="rocket">🚀</span>
                  </Button>
                </div>
              </BannerTextInner>
            </BannerText>
          </div>
        </div>
      </div>
    </BannerPart>
  );
};

export default Hero;
