
import React from 'react';

function TextField(props) {
  const { text, code, handlerChange } = props;
  return (
    <div className="form-group row">
      <label className="col-sm-4 col-form-labe">
        {text}
      </label>
      <div className="col-sm-8">
        <input
          type="text"
          name={code}
          className="form-control"
          onChange={handlerChange}
          placeholder="Enter here"
        />
      </div>
    </div>
  );
};

export default React.memo(TextField);
