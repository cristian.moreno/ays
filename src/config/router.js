import React from 'react';
import {
  HashRouter,
  Route
} from 'react-router-dom';
import Home from '../pages/Home';
import Form from '../pages/Form';

function Router() {
  return (
    <HashRouter>
      <React.Fragment>
        <Route
          exact
          path="/"
          component={Home} />
        <Route path="/form" component={Form} />
      </React.Fragment>
    </HashRouter>
  );
};

export default Router;
