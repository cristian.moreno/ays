/**
 * More info: http://chir.ag/projects/name-that-color
 */

export const Color = {
  gallery: '#F0F0F0',
  moodyBlue: '#8782ce',
  bastille: '#201C28',
  white: '#FFFFFF',
  purpleHeart: '#5338B7',
  governorBay: '#4138b7',
  codGray: '#1f1b1b',
}

export const Border = {
  lightGray: `solid 0.05rem ${Color.gallery}`,
  moodyBlue: `solid 0.25rem ${Color.moodyBlue}`,
  purpleHeart: `solid 0.1rem ${Color.purpleHeart}`,
}
