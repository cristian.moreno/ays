
import {
  FETCHING_DATA
} from './types';

export const createAction = (type, payload) => ({
  type,
  payload
});

export const generateFetch = payload => async (dispatch) => {
  dispatch(createAction(FETCHING_DATA, payload));
  try {
    const response = await fetch(`https://mathy-dot-tributi-project-baby.appspot.com/${payload.url}`, {
      method: payload.method
    });
    const json = await response.json();
    dispatch(createAction(payload.success, json));
  } catch (error) {
    dispatch(createAction(payload.failure, error));
  }
};
