import {
  FETCHING_INPUTS
} from './types';

export const fetching = () => ({
  type: FETCHING_INPUTS
});
