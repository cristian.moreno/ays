import {
    FETCHING_INPUTS,
    SET_INPUTS_SUCCESS,
    SET_INPUTS_FAILURE
} from './types';
import createReducer from '../config';

const initialState = {
  listInputs: [],
  loading: false,
  error: ''
};

const setInputs = (state = initialState, {
  payload
}) => ({
  ...state,
  listInputs: payload,
  loading: false
});

const fetching = (state = initialState) => ({
  ...state,
  loading: true
});

const setFailFetching = (state = initialState, {
  payload
}) => ({
  ...state,
  error: payload
});

const descriptor = {
  [FETCHING_INPUTS]: fetching,
  [SET_INPUTS_SUCCESS]: setInputs,
  [SET_INPUTS_FAILURE]: setFailFetching
};

export default createReducer(initialState, descriptor);