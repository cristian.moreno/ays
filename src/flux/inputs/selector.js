import {
  createSelector
} from 'reselect';

const listInputs = state => state;

const acc = (obj, keyMin, keyMax) => Object.keys(obj)
  .filter((key) => key >= keyMin && key <= keyMax)
  .map((key) => obj[key])
  .reduce((a, b) => Number(a) + Number(b), 0);

const visibility = (obj, key) => obj[key] === 'true' ? 'show' : 'hide';

export const getListInputs = createSelector(
  [listInputs],
  (inputs) => {
    const formula = {
      '1.1.4.5': (obj) => Math.max(obj['1.1.4.3'] - obj['1.1.4.4'], 0),
      '1.1.4.11': (obj) => Math.max(Number(obj['1.1.4.8']) + Number(obj['1.1.4.9']) - Number(obj['1.1.4.10']), 0),
      '1.1.4.25': (obj) => acc(obj, '1.1.4.13', '1.1.4.24'),
      '1.1.4.26.8': (obj) => acc(obj, '1.1.4.26.1', '1.1.4.26.7'),
      'default': null
    };

    const display = {
      '1.1.4.26.1': (obj) => visibility(obj, '1.1.4.26'),
      '1.1.4.26.2': (obj) => visibility(obj, '1.1.4.26'),
      '1.1.4.26.3': (obj) => visibility(obj, '1.1.4.26'),
      '1.1.4.26.4': (obj) => visibility(obj, '1.1.4.26'),
      '1.1.4.26.5': (obj) => visibility(obj, '1.1.4.26'),
      '1.1.4.26.6': (obj) => visibility(obj, '1.1.4.26'),
      '1.1.4.26.7': (obj) => visibility(obj, '1.1.4.26'),
      '1.1.4.26.8': (obj) => visibility(obj, '1.1.4.26'),
      'default': () => 'show'
    };

    return inputs.map((item) => {
      const handler = formula[item.code] || formula['default'];
      const showInput = display[item.code] || display['default'];
      return {
        ...item,
        formula: handler,
        display: showInput
      };
    });
  }
);
