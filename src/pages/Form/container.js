import React, { useState } from 'react';

import TextField from '../../components/TextField';
import Currency from '../../components/Currency';
import DropDown from '../../components/DropDown';

import { JumboTron, Title } from './style'


const renderInput = (input) => {
  const inputs = {
    text: (input) => <TextField key={input.key} {...input} />,
    currency: (input) => <Currency key={input.key} {...input} />,
    list: (input) => <DropDown key={input.key} {...input} />,
    default: (input) => {
      return (
        <h2 key={input.key}>
          {input.text}
        </h2>
      )
    },
  };
  const handler = inputs[input.type] || inputs['default'];
  return handler(input);
};

function Form({ listInputs }) {
  const [inputs, setListInputs] = useState([]);
  const [form, setForm] = useState({});

  if (listInputs !== inputs) {
    setListInputs(listInputs);
  }

  const handlerChange = (event) => {
    if (event.target.value) {
      setForm({
        ...form,
        [event.target.name]: event.target.value
      });
    }
  }
  return (
    <React.Fragment>
      <JumboTron className="jumbotron jumbotron-fluid">
        <div className="container">
          <Title className="display-4">
            Form <span role="img" aria-label="laptop">💻</span>
          </Title>
        </div>
      </JumboTron>
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <form>
              {
                inputs.map((item, index) => {
                  return renderInput({ ...item, handlerChange, form, key: index })
                })
              }
            </form>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Form;
