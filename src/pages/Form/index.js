
import {
  connect
} from 'react-redux';
import container from './container';
import {
  getListInputs
} from '../../flux/inputs/selector';

const stateProp = ({ inputs }) => {
  const { listInputs } = inputs;
  return {
    listInputs: getListInputs(listInputs)
  };
};

export default connect(stateProp, null)(container);
