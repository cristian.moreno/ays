import styled from '@emotion/styled'

import { Color } from '../../config/theme'

export const JumboTron = styled.div`
  background-color: ${Color.moodyBlue};
  font-family: "Poppins", sans-serif;
  margin-bottom: 2rem;
`
export const Title = styled.h1`
  font-family: "Anton", sans-serif;
  font-size: 100px;
  margin-bottom: 30px;
  line-height: 1.273;
  color: ${Color.codGray};;
  text-transform: uppercase;
  position: relative;
  z-index: 1;

  &::after {
    position: absolute;
    content: "";
    left: 3px;
    width: 99%;
    height: 25px;
    background-color: ${Color.moodyBlue};;
    bottom: 21px;
    z-index: -1;
  }

  @media (max-width: 991px) {
    font-size: 40px;
    margin-bottom: 15px;
    line-height: 1.3;
    display: inline-block;

    &::after {
      position: absolute;
      content: "";
      left: 0;
      width: 100%;
      height: 10px;
      background-color: ${Color.moodyBlue};;
      bottom: 8px;
      z-index: -1;
    }
  }
`
