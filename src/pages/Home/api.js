import {
  SET_INPUTS_SUCCESS,
  SET_INPUTS_FAILURE
} from '../../flux/inputs/types';

export const FETCH_GET_INPUTS = () => ({
  url: `/tax-objects/?code__startswith=1.1.4`,
  method: 'GET',
  success: SET_INPUTS_SUCCESS,
  failure: SET_INPUTS_FAILURE
});