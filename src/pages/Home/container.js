import React from 'react';
import {
  withRouter
} from 'react-router-dom'
import {
  FETCH_GET_INPUTS
} from './api';
import Hero from '../../components/Hero';

const getInputs = (props) => {
  const { fetchData, history } = { ...props }
  fetchData(FETCH_GET_INPUTS());
  history.push('/form');
};

function Home(props) {
  return (
    <Hero handler={() => getInputs(props)} />
  );
};

export default withRouter(Home);
