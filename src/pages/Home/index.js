
import {
  connect
} from 'react-redux';
import {
  fetching
} from '../../flux/inputs/actions';
import {
  generateFetch
} from '../../flux/api/actions';
import container from './container';

const dispatchProps = dispatch => ({
  fetchData(value) {
    dispatch(fetching());
    dispatch(generateFetch(value));
  }
});

export default connect(null, dispatchProps)(container);
